class JeopardyGrid extends Grid {
  constructor(
    columns,
    rows,
    cellSize,
    categoryIDs,
    jeopardyCategoryURL = "http://jservice.io/api/clues/?category="
  ) {
    super(columns, rows, cellSize);
    this.categoryIDs = categoryIDs;
    this.jeopardyCategoryURL = jeopardyCategoryURL;
    this.playerScore = 0;
    this.getCategories();
  }

  async getCategories() {
    const categoryPromises = this.categoryIDs.map(async id => {
      const res = await fetch(this.jeopardyCategoryURL + id);
      return await res.json();
    });
    this.categories = await Promise.all(categoryPromises);

    this.assignCategories(this.categories);
  }

  assignCategories(categories) {
    console.log(categories);
    for (
      let categoryIndex = 0;
      categoryIndex < categories.length;
      categoryIndex++
    ) {
      let catHeader = this.createHeader(categories, categoryIndex);
      let currentColumnElement = document.getElementById(
        "column" + categoryIndex
      );
      currentColumnElement.prepend(catHeader);

      for (let cellIndex = 0; cellIndex < 5; cellIndex++) {
        let clue = categories[categoryIndex][cellIndex];
        let clueValue = clue.value;
        // check for empty questions or answers // html elements
        if (
          clue.question === "" ||
          clue.answer === "" ||
          this.containsHTML(clue.question) ||
          this.containsHTML(clue.answer) ||
          clue.invalid_count !== 0
        ) {
          for (let curClue of categories[categoryIndex]) {
            if (curClue.value === clueValue) {
              if (
                curClue.question !== "" &&
                curClue.answer !== "" &&
                this.containsHTML(clue.question) &&
                this.containsHTML(clue.answer) &&
                clue.invalid_count === 0
              ) {
                clue = curClue;
              }
            }
          }
        }

        // replace \ and " characters
        clue.question = clue.question.replace(/\\/, "");
        clue.answer = clue.answer.replace(/"/, "");

        let currentCellElement = this.cellGrid[categoryIndex][cellIndex]
          .cellElement;

        let clueQuestionText = document.createTextNode(clueValue);
        currentCellElement.appendChild(clueQuestionText);

        this.boundClickHandler = this.unboundClickHandler.bind(
          this,
          clue,
          currentCellElement
        );
        currentCellElement.addEventListener("click", this.boundClickHandler);
        // console.log(clue.invalidCount)
      }
    }
  }

  containsHTML(text) {
    return /(<.+?>)|(&.{1,6}?;)/.test(text);
  }

  unboundClickHandler(clue, currentCellElement) {
    currentCellElement.removeEventListener("click", this.boundClickHandler);
    currentCellElement.innerHTML = "";

    this.questionDiv.innerHTML = "";

    this.questionDiv.style.display = "block";

    let clueQuestionText = document.createTextNode(
      clue.category.title + "-" + clue.question + "."
    );
    this.questionDiv.appendChild(clueQuestionText);

    let input = document.createElement("input");
    let submitButton = document.createElement("input");
    submitButton.setAttribute("type", "submit");

    this.boundSubmitHandler = this.unboundSubmitHandler.bind(this, input, clue);

    submitButton.addEventListener("click", this.boundSubmitHandler);
    input.setAttribute("type", "text");
    input.setAttribute("value", "");
    this.questionDiv.appendChild(input);
    this.questionDiv.appendChild(submitButton);
  }

  unboundSubmitHandler(input, clue) {
    if (input.value.toLowerCase() === clue.answer.toLowerCase()) {
      this.playerScore += clue.value;
      this.scoreboardUpdate();
      this.questionDiv.innerHTML = "CORRECT!";
      window.setTimeout(function timeout() {
        document.getElementById("question").style.display = "none";
      }, 2000);
    } else {
      this.playerScore -= clue.value;
      this.scoreboardUpdate();
      this.questionDiv.innerHTML = "INCORRECT! Correct answer: " + clue.answer;
      window.setTimeout(function timeout() {
        document.getElementById("question").style.display = "none";
      }, 2000);
    }
  }

  createHeader(categories, categoryIndex) {
    let catHeader = document.createElement("div");
    catHeader.className = "header";
    catHeader.innerHTML = categories[categoryIndex][0].category.title;
    catHeader.style.width = this.cellSize + "px";
    catHeader.style.height = this.cellSize / 2 + "px";
    catHeader.style.border = "1px solid";
    catHeader.style.display = "flex";
    catHeader.style.justifyContent = "center";
    catHeader.style.alignItems = "center";
    catHeader.style.textAlign = "center";
    catHeader.style.fontSize = "9px";
    catHeader.style.fontWeight = "bold";
    return catHeader;
  }

  scoreboardUpdate() {
    this.scoreDiv.innerHTML = "Score: " + this.playerScore;
  }
}
