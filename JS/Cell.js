class Cell {
  constructor(size, rowIndex, columnIndex) {
    this.height = size;
    this.width = size;
    this.columnIndex = columnIndex;
    this.rowIndex = rowIndex;

    this.cellElement = this.createCell(this.columnIndex, this.rowIndex);
  }
  swapStyles(newClass) {
    this.cellElement.className = newClass;
  }
  createCell(columnIndex, rowIndex) {
    let cellElement = document.createElement("div");
    cellElement.className = "cell";
    cellElement.id = columnIndex + " " + rowIndex;
    cellElement.style.height = this.height + "px";
    cellElement.style.width = this.width + "px";
    cellElement.style.border = this.border = "1px solid";
    cellElement.style.display = "flex";
    cellElement.style.alignItems = "center";
    cellElement.style.justifyContent = "center";
    return cellElement;
  }
}
