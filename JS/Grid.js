class Grid {
  constructor(columns, rows, cellSize) {
    this.columns = columns;
    this.rows = rows;
    this.cellSize = cellSize;
    this.boardElement = document.createElement("div");
    this.boardElement.id = "board";
    this.boardElement.style.display = "flex";
    this.boardElement.style.justifyContent = "center";

    this.cellGrid = [];
    for (let columnIndex = 0; columnIndex < this.columns; columnIndex++) {
      this.columnElement = document.createElement("div");
      this.columnElement.id = "column" + columnIndex;

      this.cellGrid.push([]);

      for (let rowIndex = 0; rowIndex < this.rows; rowIndex++) {
        let newCell = new JeopardyCell(this.cellSize, rowIndex, columnIndex);
        // console.log(newCell)
        this.cellGrid[columnIndex].push(newCell);
        this.columnElement.appendChild(newCell.cellElement);
      }
      this.boardElement.appendChild(this.columnElement);
    }

    this.questionDiv = document.createElement("div");
    this.questionDiv.id = "question";
    this.questionDiv.style.textAlign = "center";
    this.questionDiv.style.position = "absolute";
    this.questionDiv.style.display = "none";
    this.questionDiv.style.width =
      (this.cellSize + 2) * this.columns - 2 + "px";
    this.questionDiv.style.height = (this.cellSize + 2) * this.rows + "px";
    this.questionDiv.style.border = "1px solid";
    this.questionDiv.style.backgroundColor = "white";

    this.questionDiv.style.marginTop = this.cellSize / 2 + 2 + "px";

    this.boardElement.appendChild(this.questionDiv);

    this.scoreDiv = document.createElement("div");
    this.scoreDiv.id = "scoreboard";
    this.scoreDiv.innerHTML = "Score: 0";
  }

  updateDOM(destination, element) {
    document.body.appendChild(this.scoreDiv);
    destination.appendChild(element);
  }

  returnCell(columnIndex, rowIndex) {
    return this.cellGrid[columnIndex][rowIndex];
  }
}
